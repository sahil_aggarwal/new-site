/* node mailer module for sending mails*/

var nodemailer = require("nodemailer");
var request = require("request");

/**
 * Module dependencies.
 */

 
 
var mongoose = require('mongoose')
  , User = mongoose.model('User')
  , utils = require('../../lib/utils')

var login = function (req, res) {
  var redirectTo = req.session.returnTo ? req.session.returnTo : '/'
  delete req.session.returnTo
  res.redirect(redirectTo)
}

exports.signin = function (req, res) {}

/**
 * Auth callback
 */

exports.authCallback = login

/**
 * Show login form
 */

exports.login = function (req, res) {
  res.render('users/login', {
    title: 'Login',
    message: req.flash('error')
  })
}

/**
 * Show sign up form
 */

exports.signup = function (req, res) { 
   
  res.render('users/signup', {
    title: 'Sign up',
    user: new User()
  })
}

/**
 * Logout
 */

exports.logout = function (req, res) {
  req.logout()
  res.redirect('/login')
}

/**
 * Session
 */

exports.session = login

/**
 * Create user
 */

exports.create = function (req, res) {

  var ip = req.connection.remoteAddress;
  var location;
    
    var url = 'http://www.iptolatlng.com/?ip='+ip+'&position=true'
	request(url, function(err, resp, body) {
	if(!err)
	{
		var metaData = JSON.parse(body);
		var latitude = metaData.lat;
		var longitude = metaData.lng;
	
		var getLocation = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&sensor=false'
		request(getLocation, function(err, resp, body) {
		if(!err)
		{
		 location = JSON.parse(getLocation).formatted_address;		
		}
		else
		{
			location = null;
		}
			
	})
		
	}
	else
	{
		console.log(err);
	}
	  
  })	
 
  var user = new User(req.body)
  user.location = location;
  user.provider = 'local'
  user.save(function (err) {
    if (err) {
      return res.render('users/signup', {
        error: utils.errors(err.errors),
        user: user,
        title: 'Sign up'
      })
    }

    // manually login the user once successfully signed up
    req.logIn(user, function(err) {
      if (err) return next(err)
      return res.redirect('/users/'+user.id)
    })
  })
}

/**
 *  Show profile
 */

exports.show = function (req, res) {
  var user = req.profile
  res.render('users/show', {
    title: user.name,
    user: user,
	avator: "/img/avators/"+user.avator+".png"
  })
}

/**
 * Find user by id
 */

exports.user = function (req, res, next, id) {
  User
    .findOne({ _id : id })
    .exec(function (err, user) {
      if (err) return next(err)
      if (!user) return next(new Error('Failed to load User ' + id))
      req.profile = user
      next()
    })
}

/*Udate profile route*/

exports.updateProfile = function (req, res){
	var conditions = { _id:  req.user.id};
	var update = { $set: { avator: req.param('avator'), name: req.param('name'), email: req.param('email'), phonenumber: req.param('phonenumber') }};
	var options = { upsert: true };
	User.update(conditions, update, options, function(err,res){
		if(err)
		{
			console.log(err)
		}
		else
		{
			console.log(res)
		}
	});
	res.redirect('/')
	
}	

exports.forgetPassword = function (req, res)
{
	var email = req.email;
	/* send mail to user*/
	// create reusable transport method (opens pool of SMTP connections)
	var smtpTransport = nodemailer.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        user: "aggarwalsahil1992@gmail.com@gmail.com",
        pass: ""
    }
	});
	
	// setup e-mail data with unicode symbols
	var mailOptions = {
    from: "Sahil Aggarwal ✔ <aggarwalsahil1992@gmail.com>", // sender address
    to: "aggarwalsahil1992@gmail.com", // list of receivers
    subject: "Hello ✔", // Subject line
    text: "Hello world ✔", // plaintext body
    html: "<b>Hello world ✔</b>" // html body
}

	// send mail with defined transport object
	smtpTransport.sendMail(mailOptions, function(error, response){
    if(error){
        console.log(error);
    }else{
        console.log("Message sent: " + response.message);
    }

    // if you don't want to use this transport object anymore, uncomment following line
    //smtpTransport.close(); // shut down the connection pool, no more messages
	});
	
}

/*private message*/
exports.privateMessage = function(req, res){
	self = this;
	this.message = req.body.message;
	this.recieverid = req.body.recieverid;
	this.senderid = req.body.senderid;
	this.sendername = req.body.sendername;
	User.findById(recieverid, function(err,user){
		if(err)
		return res.send("messaging user error: "+err);
		//add message
		User.update({_id: self.recieverid},{$push: {"messages": {message: self.message,senderid: self.senderid,sendername: self.sendername}}}, function(err, numAffected,rawResponse)
		{
			if(err)
		    return res.send("add message error: "+err);
		});
	});
	res.redirect('/')
	
}

/*show private messages*/
exports.displayPrivateMessages = function(req,res){
	var userId = req.user.id;
	var x;
}

/*delete message*/
exports.deleteMessage = function(req,res){
	messageId = req.id;
}

/*anonymous message */
exports.anonymousmessage = function(req, res){
	self = this;
	this.message = req.body.message;
	this.email = req.body.email;
	this.senderid = req.body.senderid;
	User.findOne({email: self.email}).exec(function (err, user) {
		if(err)
		Console.log("following error occurs when searching for email "+err);
		else
		{
			if(user)
			{
				var emailBody = "Hey you have a new private message"
				User.update({_id: user._id},{$push: {"messages": {message: self.message,senderid: self.senderid}}}, function(err, numAffected,rawResponse)
				{
					if(err)
					return res.send("add message error: "+err);
				});
			}
			else
			{
			    var url;
				var emailBody = "Hey you got a new message at buzzbox checkout at " +url; 
			}
		}
	});
}

exports.messageBox = function(req,res)
{
     User.find({id: req.user.id}).populate('messages','message').exec(function (err, user){
		
		if(!err)
		{
			res.render('users/messageBox', {
			title: 'Messages',
			messages: user.messages
			})
		}
	});
 
}
