/**
 * Module dependencies.
 */

var mongoose = require('mongoose')
  , Article = mongoose.model('Article')
  , utils = require('../../lib/utils')
  , extend = require('util')._extend

/**
 * Load
 */

exports.load = function(req, res, next, id){
  var User = mongoose.model('User')

  Article.load(id, function (err, article) {
    if (err) return next(err)
    if (!article) return next(new Error('not found'))
    req.article = article
    next()
  })
}

/**
 * List
 */

exports.index = function(req, res){
  
  self = this;
  this.pages = req.param('page');
  this.location = req.user.location;
  
  Article.find().populate('user','name location avator').populate('comments.user', 'name avator').sort({'createdAt': -1}).exec(function (err, allArticles){
	if(!err)
	{
	        var articles = [];
			for (var i=0;i<allArticles.length;i++)
			{
				//if(allArticles[i].user.location == self.location)
				{
					   articles.push(allArticles[i]);
				}
			}
		var page = (self.pages > 0 ? self.pages : 1) - 1
		var perPage = 30
		var count = articles.length	
		
			res.render('articles/index', {
			title: 'Articles',
			articles: articles
			})
		
			
	}
  })
  
}


/**
 * New article
 */

exports.new = function(req, res){
  res.render('articles/new', {
    title: 'New Article',
    article: new Article({})
  })
}

/**
 * Create an article
 */

exports.create = function (req, res) {
  var article = new Article(req.body)
  article.user = req.user

  article.uploadAndSave(req.files.image, function (err) {
    if (!err) {
      req.flash('success', 'Successfully created article!')
      return res.redirect('/articles/'+article._id)
    }

    res.render('articles/new', {
      title: 'New Article',
      article: article,
      error: utils.errors(err.errors || err)
    })
  })
}

/**
 * Edit an article
 */

exports.edit = function (req, res) {
  res.render('articles/edit', {
    title: 'Edit ' + req.article.title,
    article: req.article
  })
}

/**
 * Update article
 */

exports.update = function(req, res){
  var article = req.article
  article = extend(article, req.body)

  article.uploadAndSave(req.files.image, function(err) {
    if (!err) {
      return res.redirect('/articles/' + article._id)
    }

    res.render('articles/edit', {
      title: 'Edit Article',
      article: article,
      error: utils.errors(err.errors || err)
    })
  })
}

/**
 * Show
 */

exports.show = function(req, res){
  res.render('articles/show', {
    title: req.article.title,
    article: req.article
  })
}

/**
 * Delete an article
 */

exports.destroy = function(req, res){
  var article = req.article
  article.remove(function(err){
    req.flash('info', 'Deleted successfully')
    res.redirect('/articles')
  })
}
/*blank index page*/
exports.blankIndex =  function(req ,res){
	res.render('articles/blankIndex')
}

exports.like = function(req, res){
	var body = req.body;
}